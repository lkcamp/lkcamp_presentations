.. role:: cover-title

.. role:: cover

:cover-title:`RCU`
##################

.. space:: 150

.. class:: cover

   LKCAMP

   Sep 01, 2020

.. raw:: pdf

   PageBreak standardPage


What is RCU?
============

* Synchronization mechanism
* Allows concurrent read and write
* No locking (for readers)
* Three parts:

  * "Hide" item
  * Wait for last usage (*aka* Grace period)
  * Destroy item


RCU timeline
============

.. image:: img/rcu-timeline.png
   :width: 15cm

Inspired by image from [1].


When to use RCU
===============

* Concurrent access to strcuture
* Read-heavy

  * Few writers
  * Multiple readers

* Performance is critical

  * Low overhead


List replacement example code (struct)
======================================

.. code-block:: c

   struct foo {
            struct list_head list;
            int a;
            int b;
            int c;
   };
   LIST_HEAD(head);

Source: [1]


List replacement example code
=============================

.. code-block:: c

   p = search(head, key);
   if (p == NULL) {
            /* Take appropriate action, unlock, and return. */
   }
   q = kmalloc(sizeof(*p), GFP_KERNEL);
   *q = *p;
   q->b = 2;
   q->c = 3;
   list_replace_rcu(&p->list, &q->list);
   synchronize_rcu();
   kfree(p);

Source: [1]


List replacement 
================

.. image:: img/list_replace1.jpg
   :width: 15cm

Source: [1]


List replacement 
================

* ``q = kmalloc(sizeof(*p), GFP_KERNEL);``

.. image:: img/list_replace2.jpg
   :width: 15cm

Source: [1]


List replacement 
================

* ``*q = *p;``

.. image:: img/list_replace3.jpg
   :width: 15cm

Source: [1]


List replacement 
================

* ``q->b = 2;``

.. image:: img/list_replace4.jpg
   :width: 15cm

Source: [1]


List replacement 
================

* ``q->c = 3;``

.. image:: img/list_replace5.jpg
   :width: 15cm

Source: [1]


List replacement 
================

* ``list_replace_rcu(&p->list, &q->list);``

.. image:: img/list_replace6.jpg
   :width: 15cm

Source: [1]


List replacement 
================

* ``synchronize_rcu();``

.. image:: img/list_replace7.jpg
   :width: 15cm

Source: [1]


List replacement 
================

* ``kfree(p);``

.. image:: img/list_replace8.jpg
   :width: 15cm

Source: [1]


How it works
============

* Publish-subscribe mechanism

  * Needed due to reordering optimizations

* Grace period end detection
  
  * No locking?!


Publishing
==========

Normal:

.. code-block:: c

   p = kmalloc(sizeof(*p), GFP_KERNEL);
   p->a = 1;
   p->b = 2;
   p->c = 3;
   gp = p;

RCU:

.. code-block:: c

   p = kmalloc(sizeof(*p), GFP_KERNEL);
   p->a = 1;
   p->b = 2;
   p->c = 3;
   rcu_assign_pointer(gp, p);

Source: [1]


Subscribing
===========

Normal:

.. code-block:: c

   p = *gp;
   if (p != NULL) {
            do_something_with(p->a, p->b, p->c);
   }

RCU:

.. code-block:: c

   rcu_read_lock();
   p = rcu_dereference(gp);
   if (p != NULL) {
            do_something_with(p->a, p->b, p->c);
   }
   rcu_read_unlock();

Source: [1]


Publish-subscribe for lists
===========================

Publish:

.. code-block:: c

   p = kmalloc(sizeof(*p), GFP_KERNEL);
   p->a = 1;
   p->b = 2;
   p->c = 3;
   list_add_rcu(&p->list, &head);

Subscribe:

.. code-block:: c

   rcu_read_lock();
   list_for_each_entry_rcu(p, head, list) {
     do_something_with(p->a, p->b, p->c);
   }
   rcu_read_unlock();

Source: [1]


Grace period end detection
==========================

* While in RCU read-side critical section, can't

  * block
  * switch to user-mode execution
  * enter the idle loop

* When all CPUs did any of that, grace period is over! (Exception:
  ``PREEMPT_RT``)


Simplified RCU implementation
=============================

.. code-block:: c

   void rcu_read_lock() {
           preempt_disable[cpu_id()]++;
   }

   void rcu_read_unlock() {
           preempt_disable[cpu_id()]--;
   }

   void synchronize_rcu(void) {
           for_each_cpu(int cpu)
                   run_on(cpu);
   }

Source: [3]

* In reality, ``synchronize_rcu`` waits for context switches on all CPUs.


RCU in the wild
===============

``git grep -l "rcu_read_lock" | sed 's|\(.*\)/.*|\1|' | sort | uniq -c | sort -n -r``

.. code-block::

   47 net/ipv4
   41 include/linux
   36 net/ipv6
   31 net/mac80211
   30 net/core
   27 net/netfilter
   26 kernel
   25 mm
   24 include/net
   24 fs
   20 net/batman-adv
   20 drivers/net/wireless/intel/iwlwifi/mvm
   17 fs/nfs
   17 drivers/md

RCU in the wild
===============

.. code-block::

   15 lib
   15 drivers/infiniband/hw/hfi1
   14 kernel/bpf
   13 net/sched
   13 net/bridge
   13 drivers/net
   13 block
   13 arch/powerpc/kvm
   12 kernel/sched
   12 drivers/gpu/drm/i915
   12 Documentation/RCU
   11 net/rds
   11 drivers/infiniband/core
   10 security/apparmor
   10 net/sunrpc
   10 fs/btrfs
   10 fs/afs
   ...


RCU type of usage
=================

* In 3.16 kernel

==================================== =========
Type of Usage                        API Usage
==================================== =========
RCU critical sections                4,431
RCU dereference                      1,365
RCU synchronization                  855
RCU list traversal                   813
RCU list update                      745
RCU assign                           454
Annotation of RCU-protected pointers 393
==================================== =========


RCU type of usage
=================

==================================== =========
Type of Usage                        API Usage
==================================== =========
Initialization and cleanup           341
RCU lockdep assertion                37
Total                                9,434
==================================== =========

Source: [3]


License
=======

This work is licensed under a `Creative Commons "Attribution-ShareAlike 4.0
International"`_ license.

.. _Creative Commons "Attribution-ShareAlike 4.0 International":
   https://creativecommons.org/licenses/by-sa/4.0/deed.en


Bibliography
============

[1] What is RCU, Fundamentally?.
https://lwn.net/Articles/262464/.
Accessed: 2020-08

[2] RCU Concepts.
https://www.kernel.org/doc/html/latest/RCU/rcu.html.
Accessed: 2020-08

[3] RCU Usage In the Linux Kernel: One Decade Later.
https://pdos.csail.mit.edu/6.828/2017/readings/rcu-decade-later.pdf.
Accessed: 2020-08
