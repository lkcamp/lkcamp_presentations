%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% I, the copyright holder of this work, release this work into the
%% public domain. This applies worldwide. In some countries this may
%% not be legally possible; if so: I grant anyone the right to use
%% this work for any purpose, without any conditions, unless such
%% conditions are required by law.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}
\usetheme[faculty=science, university=uu, logo=uu-logo]{fibeamer}
\usepackage[utf8]{inputenc}
\usepackage[
  main=english
]{babel}
\title{Device Drivers} %% that will be typeset on the
\subtitle{Meeting 3} %% title page.
\author{LKCAMP - Round 4 - Apr 28, 2020}
\usepackage{ragged2e}  % `\justifying` text
\usepackage{booktabs}  % Tables
\usepackage{tabularx}
\usepackage{tikz}      % Diagrams
\usetikzlibrary{calc, shapes, arrows, backgrounds}
\usepackage{amsmath, amssymb}
\usepackage{url}       % `\url`s
\usepackage{listings}  % Code listings

\usepackage[
    type={CC},
    modifier={by-sa},
    version={4.0},
]{doclicense}

\tikzstyle{block} = [rectangle, draw, text centered, minimum width=2cm]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{dline} = [draw, latex'-latex']

\frenchspacing
\begin{document}
  \frame{\maketitle}

%% AQUI COMEÇA O CONTEÚDO

  \AtBeginSection[]{% Print an outline at the beginning of sections
    \begin{frame}<beamer>
      \frametitle{Contents}
      \tableofcontents[currentsection]
    \end{frame}}

    \section{Introduction}

    \begin{frame}{Device Driver}
        \begin{itemize}
            \item \textbf{A program that operates a device.}

            \item Interface between hardware and the OS \\(and any other application accessing it).

            \item Abstracts the details of how the device works.
        \end{itemize}
    \end{frame}

    \begin{frame}{Driver: from kernel to device}
        \begin{figure}[!htb]
            \centering
            \input{tkz/device_driver.tex}
        \end{figure}
    \end{frame}

    \begin{frame}{So much driver code in the kernel}
        \begin{figure}[!htb]
            \centering
            \includegraphics[width=.6\textwidth]{img/drivers_vs_subsystems.png}
            \label{fig:}
        \end{figure}

        Source:
        \href{https://github.com/satoru-takeuchi/linux-kernel-statistics}{https://github.com/satoru-takeuchi/linux-kernel-statistics}

        {\tiny * Data from 2017.}
    \end{frame}


    \begin{frame}{There are always new drivers}
        \begin{figure}[!htb]
            \centering
            \includegraphics[width=.9\textwidth]{img/new_vs_old_drivers.png}
            \label{fig:}
        \end{figure}

        Source:
        \href{https://github.com/satoru-takeuchi/linux-kernel-statistics}{https://github.com/satoru-takeuchi/linux-kernel-statistics}

        {\tiny * Data from 2017.}
    \end{frame}

    \section{How a device driver works}

    \subsection{File API}

    \begin{frame}{File as an API}
        \begin{itemize}
            \item Normally we think of a file as a container of information
                saved on disk, but that's not always true!
            \item File is an API (an interface for programs). The most common
                actions of that interface are:
                \begin{itemize}
                    \item Open
                    \item Read
                    \item Write
                    \item Close
                \end{itemize}
            \item Device files can be created to, instead of storing
                information on disk, communicate with the kernel through those
                actions!
        \end{itemize}
    \end{frame}

    \begin{frame}{Device files}
    \begin{itemize}
        \item On UNIX-like systems (like Linux), ``Everything is a file''.
        \item Device files can be used to:
            \begin{itemize}
                \item Query information from the kernel
                \item Send information to the kernel
                \item Query information from a driver
                \item Send information to a driver
            \end{itemize}
        \item For example, you can get information about your CPU from the kernel by
            reading `/proc/cpuinfo', with \texttt{cat /proc/cpuinfo}. That file doesn't exist in your disk!
    \end{itemize}
    \end{frame}

    \subsection{Major and minor}
    \begin{frame}{Major and minor numbers}
        \begin{itemize}
            \item Since device files are used to communicate with the drivers,
                there must be a way to know which files correspond to which
                drivers: major and minor numbers.
            \item The major number identifies the driver.
            \item The minor number identifies the device (a single instance)
                controlled by the driver.
        \end{itemize}
    \end{frame}

    \begin{frame}{Major and minor example}
    \begin{figure}[overlay,remember picture]
        \includegraphics[width=10cm]{img/major-minor.png}
    \caption{ls -l /dev/video*}
    \end{figure}%
    \begin{itemize}
        \item These two files represent two video devices, like two cameras.
        \item Both cameras use the same driver (major 81).
        \item Each camera is a separate camera (minors 0 and 1).
    \end{itemize}
    \end{frame}

    \subsection{Device type}
    \begin{frame}{Device type}
    \begin{itemize}
        \item Devices can have one of three types:
        \begin{itemize}
            \item Block: used for raw storage (\emph{eg} disk)
            \item Network: used for network interfaces
            \item Character: used for most devices
        \end{itemize}

        \item The device type changes which file operations can be implemented.
    \end{itemize}
    \end{frame}

    \begin{frame}{Defining the driver behavior}
    \begin{itemize}
        \item The driver needs a way to tell the kernel what happens when a file
            operation is called on a file with its major number.
        \item This is done by hooking callbacks (function pointers) in
            \texttt{struct file\_operations}.
    \end{itemize}
    \end{frame}

    \begin{frame}[fragile]{The struct file\_operations}
        \begin{lstlisting}
struct file_operations {
	ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
	ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
	__poll_t (*poll) (struct file *, struct poll_table_struct *);
	int (*mmap) (struct file *, struct vm_area_struct *);
	int (*open) (struct inode *, struct file *);
	int (*flush) (struct file *, fl_owner_t id);
	int (*release) (struct inode *, struct file *);
	int (*lock) (struct file *, int, struct file_lock *);
	...
};
        \end{lstlisting}
    \end{frame}

    \begin{frame}{Device file creation}
    \begin{itemize}
        \item Userspace needs to create device file.
        \item Some applications that do that:
            \begin{itemize}
                \item udev
                \item mknod
            \end{itemize}
    \end{itemize}
    \end{frame}

    \begin{frame}{Creating a device file - mknod}
    \begin{figure}[overlay,remember picture]
        \includegraphics[width=.8\textwidth]{img/man-mknod.png}
    \caption{man mknod}
    \end{figure}%
    \begin{itemize}
        \item Example:
        \begin{itemize}
            \item[] \$ \textbf{mknod /dev/my-device c 42 0}
        \end{itemize}
    \end{itemize}
    \end{frame}


    %\begin{frame}{}
    %    %TODO add schematic of whole process. from echo to major,minor, to
    %    %driver
    %\end{frame}

    \section{Modules}
    \subsection{Motivation}

    \begin{frame}{Why use modules?}
    \begin{itemize}
        \item With so many different drivers on the Linux kernel, an image
            containing all of them would be wasteful.
        \item But the image can be compiled containing only the necessary
            drivers to the target system.
        \item But most users don't want to have to compile their own kernel.
        \item In this scenario, how can the Linux distros provide a pre-built
            kernel, with all the necessary drivers for the system to function,
            but without bundling all of them, given that each user needs a
            different combination of drivers?
        \item Modules!
    \end{itemize}
    \end{frame}

    \begin{frame}{Solution using modules}
    \begin{itemize}
        \item The solution to this problem is to compile each driver as a
            separate module.
        \item When the user downloads the Linux kernel pre-built by its
            preferred distro, it comes with all drivers pre-compiled but
            separate from the main image.
        \item When the kernel is loaded, only the modules containing the drivers
            needed for the system are loaded.
        \item Now the user can have a pre-built Linux image that is only big
            enough for the needed modules!
    \end{itemize}
    \end{frame}

    \subsection{Characteristics}
    \begin{frame}{Modules}
    \begin{itemize}
        \item A module:
        \begin{itemize}
            \item is a piece of compiled kernel code;
            \item \textbf{acts as an extension (plugin) of the kernel};
            \item can be loaded/unloaded without reboot;
            \item can be loaded from user space;
            \item runs in kernel mode.
        \end{itemize}
        \item Another benefit of a module is that it can be loaded only while
            in use: it can be loaded when a device is connected and unloaded
            when disconnected.
    \end{itemize}
    \end{frame}

    \begin{frame}{Installed modules}
    \begin{itemize}
        \item The extension for modules is .ko.
        \item They are usually located at \alert{/lib/modules/\$(uname -r)/}:
    \end{itemize}

    \begin{figure}[overlay,remember picture]
        \includegraphics[width=6cm]{img/lib-mod.png}
    \caption{\$ tree /lib/modules/\$(uname -r)/kernel}
    \end{figure}%

    \end{frame}

    \begin{frame}{Utility commands}
    \begin{itemize}
        \item \textbf{insmod <file.ko>} \\
        Loads module (without dependencies)
        \vspace{1cm}

        \item \textbf{modprobe <module>} \\
        Loads module from /lib/modules/\$(uname -r) \\
        and its dependencies
        \vspace{1cm}

        \item \textbf{rmmod <module>} \\
        Unloads a module
    \end{itemize}
    \end{frame}

    \begin{frame}{Utility commands}
    \begin{itemize}

        \item \textbf{lsmod} \\
        Lists loaded modules
        \vspace{0.3cm}

        \item \textbf{modinfo <module>} \\
        Prints general information of a module
        \vspace{0.3cm}

        \item \textbf{dmesg} \\
        Prints kernel log messages

        \begin{figure}[overlay,remember picture]
        \includegraphics[width=10cm]{img/modinfo.png}
        \caption{modinfo}
        \end{figure}%
    \end{itemize}
    \end{frame}

    \defverbatim[colored]\sleepSort{
    \begin{lstlisting}[language=C,tabsize=2]
    #include <linux/init.h>
    #include <linux/module.h>

    static int __init hello_init(void) {
            printk(KERN_INFO "Hello World\n");
            return 0;
    }

    static void __exit hello_exit(void) {
            printk(KERN_INFO "Bye World\n");
    }

    module_init(hello_init);
    module_exit(hello_exit);
    \end{lstlisting}}
    \begin{frame}{Hello World}{A module example code}
      \sleepSort
    \end{frame}

    \subsection{Out-of-tree vs. In-tree}
    \begin{frame}{Compiling a Module}
    \begin{itemize}
        \item \textbf{out-of-tree} \\
        Kernel source code is not required
        \vspace{1cm}

        \item \textbf{in-tree} \\
        Code needs to be placed in the kernel’s code tree
    \end{itemize}
    \end{frame}

    \begin{frame}{Out-of-tree}
    \textbf{No Kernel source code is needed} \\
    \vspace{1cm}
    Requires: \\
    \begin{itemize}
        \item \textbf{linux-headers-\$(uname -r)} \\
        so the modules can access functions from kernel
        \vspace{0.5cm}
        \item \textbf{Makefile} \\
        echo “obj-m+=hello.o” > Makefile
    \end{itemize}
    \end{frame}

    \begin{frame}{In-tree}
    \begin{itemize}
        \item \textbf{Inside a sub-directory of the Linux Kernel} \\
        E.g.: \\
        \$ ls drivers/misc/hello/ \\
        hello.c Makefile
        \vspace{1cm}

        \item \textbf{Compiled with the Kernel code}
        \vspace{1cm}

        \item Can be \alert{loadable} or \alert{built-in}.
    \end{itemize}
    \end{frame}

    \begin{frame}{Loadable module}
    \begin{itemize}
        \item \textbf{Can be loaded in run-time (same as out-of-tree)}
        \item Building as loadable: \\
        \$ cat drivers/misc/hello/Makefile \\
        ... \\
        obj-m+=hello.o
    \end{itemize}
    \end{frame}

    \begin{frame}{Built-in}
    \begin{itemize}
        \item \textbf{Compiled code is loaded with the Kernel (at boot)}
        \item Building as built-in: \\
        \$ cat drivers/misc/hello/Makefile \\
        ... \\
        obj-y+=hello.o \\
    \end{itemize}
    \end{frame}

    \begin{frame}{Making it optional}
    \textbf{Add variable for module/built-in}
    \begin{itemize}
        \item Building as a \$(user choice) \\
        \$ cat drivers/misc/hello/Makefile \\
        ... \\
        obj-\$(CONFIG\_HELLO)+=hello.o
    \end{itemize}
    \end{frame}

    \begin{frame}{Making it optional}
        \begin{figure}[overlay,remember picture]
        \includegraphics[width=10.5cm]{img/make-menuconfig.png}
        \caption{\$ make menuconfig}
        \end{figure}%
    \end{frame}

    \section{Char driver example}

    \begin{frame}[fragile]{Driver example code}
        \begin{lstlisting}[language=C,tabsize=8]
static ssize_t turn_on_led(struct file *file, char __user *buf,
			   size_t count, loff_t *offset)
{
	make_led_turn_on();
	return 0;
}

struct file_operations fops = {
	.read = turn_on_led,
};

static int led_init(void)
{
	register_chrdev(42, KBUILD_MODNAME, &fops);
	return 0;
}
        \end{lstlisting}
    \end{frame}

    \begin{frame}{Example driver out-of-tree compilation}
        \$ \textbf{ls} \\
        hello.c  Makefile \\
        \vspace{0.5cm}

        \$ \textbf{make -C /lib/modules/\$(shell uname -r)/build/ M=\$(PWD) modules}
        \vspace{0.5cm}

        \$ \textbf{ls} \\
        hello.c  \textbf{hello.ko}  hello.mod.c  hello.mod.o  hello.o  \\
        Makefile  modules.order  Module.symvers
        \vspace{0.5cm}

        \$ \textbf{sudo insmod hello.ko}
    \end{frame}

    \begin{frame}{Device file example}
        \$ \textbf{mknod /dev/led c 42 0}
        \vspace{0.5cm}

        \$ \textbf{cat /dev/led}
        \vspace{0.5cm}

        Led turns on!
    \end{frame}

    \begin{frame}{We only saw part of the story}
        \begin{figure}[!htb]
            \centering
            \input{tkz/dd_vs_syscall.tex}
        \end{figure}
    \end{frame}

    % ------------------------- END OF CHAR DEVICES
    % BIB

    \begin{frame}[fragile]
    \frametitle{License}
    \doclicenseThis
    \end{frame}

    % BIB
    \begin{frame}{Bibliography}
    \nocite{*}
    \bibliographystyle{plain}
    \bibliography{m3}
    \end{frame}

\end{document}
